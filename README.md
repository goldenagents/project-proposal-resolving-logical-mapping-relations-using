This tutorial makes use of [SPARQL](https://www.stardog.com/tutorials/getting-started-1/) to query [RDF graphs](https://www.cambridgesemantics.com/blog/semantic-university/learn-rdf/) provided by the [Golden Agents](https://www.goldenagents.org/) project.
## Project Description
In this project, you will investigate formal query rewriting rules to automatically translate concepts from one ontology to another at query-time, according to the formal relations specified in mapping files, and implement these rules as capabilities of the agents.

In the Golden Agents project, we use a multi-agent system to query and combine data from multiple data sources to answer a user query spanning across the domain of those sources. These data sources use differently structured data, according to their ontologies. To understand what concepts in different ontologies refer to the same things, the agents use mappings, which express the relations between these concepts using constructs from formal logics. Users unfamiliar with the individual data sources can write queries using the vocabulary of one global ontology, the Golden Agents (ga) ontology, and the system manages the translation and distribution of those queries and aggregation of results.

The queries are written in SPARQL, the de facto query language for the semantic web, and the data sources are represented as graphs in RDF (Resource Description Framework). Knowledge of RDF and SPARQL is useful, but not a prerequisite.

Both SPARQL and RDF are heavily grounded in formal logic, and make use from specifications from OWL (Web Ontology Language). OWL specifies a decidable subset of Description Logic (DL), that is used to compute the logical entailments of the formal statements specified in an ontology or a mapping file using a ‘reasoner’. These reasoners are generally slow. In your research you will investigate simplifying a subset of the OWL language to query rewriting rules, that can be used by the agents as they request data from the individual sources. You will ground these rules in the formal logic, and implement these rules in the agents.

## Prerequisites
The tutorial makes use of slightly more advances concepts from [Web Ontology Language (OWL)](https://www.w3.org/OWL/), which provides reasoning constructs on top of RDF statements in the form of Description Logic (which is itself a decidable subset of first order logic).
For these examples, it is necessary to understand:
* How *ontologies* are used to specify the structure of a graph database
* The definition of triples and triple patterns (and their distinction)
* That a SPARQL query matches an RDF graph against a pattern of triples with fixed and variable values.
* What the *projection* of a query means, specifically, the difference between a SELECT and a CONSTRUCT projection
* What *materializing* logical entailments on an RDF graph means

Background reading:
 * https://www.cs.vu.nl/~guus/public/owl-restrictions/

We will use the `ga` prefix to denote concepts from the Golden Agents Ontology (e.g. `ga:CreativeAgent` or `ga:authorOf`), and use prefixes as defined in the GGD mapping file to represent actual name spaces from the GGD graph. For fictional examples, the example prefix `ex` will be used.

## Resources used
All examples make use of the Gelegenheidsgedichten (GGD) database curated by the Golden Agents project. All CONSTRUCT queries can be evaluated on the Golden Agents SPARQL endpoint:
* SPARQL endpoint: https://sparql.goldenagents.org/
* Enter the following IRI in the field for Default Graph to test the example queries: `https://data.goldenagents.org/datasets/ufab7d657a250e3461361c982ce9b38f3816e0c4b/ggd_20210408`

A visualisation of the Golden Agents (GA) Ontology to which the GGD is mapped can be found here:
* http://www.visualdataweb.de/webvowl/#iri=https%3A%2F%2Fsparql.goldenagents.org%2F%3Fdefault-graph-uri%3Dhttps%3A%2F%2Fdata.goldenagents.org%2Fdatasets%2Fufab7d657a250e3461361c982ce9b38f3816e0c4b%2Fga_ontology_20210421%26format%3Dtext%252Fplain%26query%3DCONSTRUCT%2520%257B%253Fx%2520%253Fy%2520%253Fz%2520%257D%2520WHERE%2520%257B%253Fx%2520%253Fy%2520%253Fz%257D

Examples are based on relations given in the mapping from the GGD to the GA Ontology. The full mapping file is available at https://github.com/knaw-huc/golden-agents-datasets/blob/main/2021_Q1/mapping/ggd2ga.trig, and can be used to gain an understanding of what these files look like.

## How the Golden Agents resolve mappings
Currently, each database is managed by a single data source agent, and a central broker agent splits a user query into the triple patterns. Each data source agent, upon receiving its set of triple patterns, uses the mapping to translate all known concepts (IRI's denoting classes or properties) at any position in the triple patterns to the corresponding concept in the RDF graph the data source agent manages. It then formulates a CONSTRUCT query, with the concepts of the GA Ontology in the head of the query, and the translated concepts in the RDF graph in the body. The result of a CONSTRUCT query is a new graph database, which can also be used to evaluate other SPARQL queries on. By creating the CONSTRUCT queries in this manner, they can be evaluated on the local data sources, but the resulting graph contains only terminology used in the Golden Agents ontology, meaning the original user query can be evaluated on the resulting graph.

### Mappings
A mapping in the context of the Golden Agents project is a file defining formal relations between concepts. In essence, it is a way of *merging* two ontologies: An ontology defines what concepts are available (classes) and how these concepts are interrelated (properties). E.g. some class `A` is a subclass of `B`, and `A` always has a property `p`, which always is of the type of a third class `C`. Moreover, an ontology allows specifying *reasoning rules*, which serve to generalize explicit statements in the ontology. For example, instead of explicitly defining the grandfather relation, a rule "If `x` has a father `y`, and `y` has a father `z`, then `z` is a grandfather of `x`" or "If `z` is a grandfather of `x` and `x` has a gender `male`, `x` is a grandson of `y`".

A mapping uses exactly the same mechanics to relate concepts. However, instead of defining new concepts, and specifying relations on those concepts within some domain, it relates concepts *across* ontologies. In the context of the Golden Agents project, for example, a mapping is able to state that the class `Painting` from a database about paintings is a subclass of the class `CreativeWork` from another database about objects in a museum. Because the mechanics are the same, we can infer that every property that holds for a painting occuring in the first database also holds for the same CreativeWork occuring in the second.

In fact, because the mechanics of ontologies and mappings are the same, and both are expressed in the same data structure as the graph itself (i.e. RDF), we can merge both ontologies and the mapping file, and the result would be one ontology encompassing both original ontologies.

### Golden Agents example
As an example, the following (fictitious) mapping could be envisioned from a knowledge graph containing the fictitious concepts `ex:Woman` and `ex:Man`, and also using the common concepts [`schema:CreativeWork`](https://schema.org/CreativeWork) (a class) and [`schema:author`](https://schema.org/author) (a property) to the Golden Agents ontology. 

Note that in this example, and following examples, the semantics of all concepts prefixed with [`schema`](https://schema.org/docs/full.html) or [`owl`](https://www.w3.org/TR/owl-ref/) are rigorously defined (the keyword `a` is semantically equivalent to `rdf:type`) and can thus be looked up.

```rdf
ex:Woman owl:subClassOf ga:Person .
ex:Man owl:subClassOf ga:Person .
schema:CreativeWork owl:equivalentClass ga:CreativeWork .
schema:author owl:inverseOf ga:authorOf
```

A user could ask the following query (expressed using the Golden Agents ontology), that cannot be answered by the knowledge graph, which does not use the concepts used in this query:
```sparql
SELECT * WHERE {
    ?work a ga:CreativeWork .
    ?author a ga:Person .
    ?author ga:authorOf ?work
}
```

Using the mapping, we can construct a new temporary data source, containing a translated subset of the original data source, with the following CONSTRUCT query:

```sparql
CONSTRUCT {
    ?work a ga:CreativeWork .
    ?author a ga:Person .
    ?author ga:authorOf ?work
} WHERE {
    ?work a schema:CreativeWork .
    ?work schema:author ?author .
    {?author a schema:Man}
    UNION
    {?author a schema:Woman}
}
```

We create a triple `?work a ga:CreativeWork` (automatically making the triple use the concept from the GA Ontology) for all variables `?work` that match the triple pattern `?work a schema:CreativeWork` in the knowledge graph. We can make this translation because from the fourth line of the fictitious mapping, we know both classes are equivalent.

Similarly, we create a triple `?author ga:authorOf ?work` (again using a concept from the GA ontology) based on the fourth line in the fictitious mapping, but because `schema:author` is the inverse of `ga:authorOf` (this property is symmetric), we need to switch the subject and object of the `schema:author` property. 

Lastly, although we do not have a class that matches the class `ga:Person`, we do know that all entities belonging to at least one of `ex:Man` or `ex:Woman` in the knowledge graph have to belong to the `ga:Person` class. For this reason, we create a triple pattern `?author a ga:Person` for all entities belonging to either class in the knowledge graph (using `UNION` in the body of the query). 

Both variables `?work` and `?author` occur in more than one triple pattern, meaning the above statement that we instantiate a triple for each instance matching the variable in the corresponding triple pattern is not true: In reality, we only instantiate those triples for variables that match *all* triple patterns they occur in.

The original (example) query can be evaluated on the graph resulting from the above CONSTRUCT query.

## Research Question
The translations performed at query-time as explained above are relatively simple substitutions. However, an increasingly large number of mappings in the Golden Agents project make use of more complex mapping statements, as relations between the mapped data sources and the GA ontology cannot be expressed any longer in simpel subtitution terms.

So far, only equivalence and subclass/sub property relations have been used in the project. However, OWL has a very rich language allowing specifications of much more complex relations. For example, property paths and chains can be used, relations can be subject to restrictions or conditions, or domain and range information may be used. 
We will go over some examples used in the GGD mapping. However, your research will focus on finding resolutions for more mappings than just these examples, generalizing them, and implementing them in the data source agents of Golden Agents infrastructure.

### Property Paths
A property path is a path through the graph that can be traversed. For example, the GDD mapping defines the following relation:
```rdf
ga:participationOf owl:equivalentProperty sem:hasActor/rdf:value
```

Meaning that for some entity that, in the GA ontology has some property `participationOf`, that value corresponds to finding the value of the property `rdf:value` of the entity that is the value of `sem:hasActor`.

This query still is a simple substitution, as SPARQL is able to understand property paths:

```sparql
 SELECT * WHERE {
    ?subj ga:participationOf ?obj
 }
```

```sparql
PREFIX ga: <https://data.goldenagents.org/>
PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> 

CONSTRUCT {
    ?subj ga:participationOf ?obj
} WHERE {
    ?subj sem:hasActor/rdf:value ?obj
}
```

### Reversed Property Paths
However, a property path could go the other way around as well. Consider the following statement in the GGD mapping:

```rdf
schema:about owl:equivalentProperty ga:hasContent/ga:isAbout
```

This means the Golden Agents ontology would expect some entity that is the object of `ga:hasContent` and has a value for `ga:isAbout`. However, in the GGD, this entity does not exist. We solve this by introducing a [blank node](https://en.wikipedia.org/wiki/Blank_node). 

```sparql
PREFIX ga: <https://data.goldenagents.org/>
PREFIX   schema: <http://schema.org/>

CONSTRUCT {
   ?something ga:hasContent _:blank1 .
   _:blank1 ga:isAbout ?object
} WHERE {
   ?something schema:about ?object 
}
```

This will allow finding the value of `ga:isAbout` of some `ga:CreativeWork`, but not the value `ga:hasContent` of that work. This is not a problem, as that information simply is not present.

Additionally, from the Golden Agents ontology, we can infer that the blank node `_:blank1` has to be an instance of `ga:Idea`, as the [domain of](https://github.com/knaw-huc/golden-agents-datasets/blob/7fc52e6a72f835f68ca9a538a35f3baafc156fd7/2021_Q1/ontology/ontology-ga.ttl#L74-L76) `ga:isAbout` is `ga:Idea`, as is the [range of](https://github.com/knaw-huc/golden-agents-datasets/blob/7fc52e6a72f835f68ca9a538a35f3baafc156fd7/2021_Q1/ontology/ontology-ga.ttl#L68-L71) `ga:hasContent`.

This information could be leveraged to make the translation even more precise.

### Conditions on relations
Some mappings are more complex than the examples given so far. For example, the `ga:Writer` class is mapped to the GGD as follows:
```rdf
ga:Writer owl:equivalentClass [ rdf:type owl:Restriction ; 
                                 owl:onProperty [ owl:inverseOf schema:author ] ; 
                                 owl:someValuesFrom schema:Role ] .
```

For clarity, this notation is shorthand for introducing a restriction by means of a blank node. The above expression can be rewritten as:

```sparql
ga:Writer owl:equivalentClass _:blank_class .
_:blank_class rdf:type owl:Restriction .
_:blank_class owl:onProperty _:blank_property .
_:blank_property owl:inverseOf schema:author .
_:blank_class owl:someValuesFrom schema:Role
```
Since SPARQL matches parts of the graph the query is being evaluated on, and is able to resolve logical entailments, statements like this can be used in the query. For example, the query...

```sparql
SELECT * WHERE {
    ?x a ga:Writer
}
```

...could *in theory* be resolved using the following CONSTRUCT query (but read on, in practice we may need a different solution):

```sparql
prefix  schema: <http://schema.org/> 
prefix  ga: <https://data.goldenagents.org/ontology/>

CONSTRUCT {
    ?x a ga:Writer
} WHERE {
    ?x a [ rdf:type owl:Restriction ; 
                                 owl:onProperty [ owl:inverseOf schema:author ] ; 
                                 owl:someValuesFrom schema:Role ]
}
```

However, this requires reasoning out these statements to their logical conclusions, which requires a *reasoner* that not all SPARQL endpoints support. The listed CONSTRUCT query does not provide any results on our example endpoint for this very reason.

So why does the mapping require such complex statements anyway?  As our mapping expert explains:

>The problem is that we cannot simply state that every schema:Person in the GGD data, is a `ga:Writer`, since the `schema:Person` can be an author, or a participant in an event. Therefore, we can at most say that every `schema:Person` is a `ga:Substantial`, and/or a `ga:Person`. 
To also capture the `ga:Writer`, this can either follow from a mapping in which you add a conditional (as shown), or from the fact that a `schema:Person` is in the rdfs:domain of a ga:writerOf.

A representation of the ontology was also provided

![Writer in the Golden Agents Ontology](thumbnail_Outlook-4ooieo1h.png "Writer in the Golden Agents Ontology")

Since the mapping is just a formal description of a constraint on the data, all writers can be found with a simple conjunctive query as well. In order to create this query, it is helpful to understand what the mapping actually means. Again from our mapping expert:

> The conditional mapping with the owl:Restriction states that every resource that occurs as subject on a ^schema:author property (there is no inverse equivalent in schema.org) that refers to an instance of type schema:Role is a ga:Writer. I think your verbose expression is correct. 

With this explanation, the following query can be created:

```sparql
PREFIX ga: <https://data.goldenagents.org/>
PREFIX   schema: <http://schema.org/>

CONSTRUCT {
    ?writer a ga:Writer
} WHERE {
     ?something schema:author ?writer .    # Every resource that occures as a subject on a ^schema:author property
     ?something a schema:Role              # That refers to an instance of type schema:Role
}
```

### The research complexity
All above examples had solutions that were hand-crafted. In order to use current and future mapping files, solutions should be generalized, at least within some subclass of the OWL language.

Moreover, all example queries were very small, and used only concepts in the mapping statement under discussion. In practice, queries consist of a number of triple patterns, each of which need to be translated, so the body of the CONSTRUCT query will be a conjunction of complex BGP's. In order to generalize the approach, formal logic can be used to prove (or at least verify) that combining the mappings in the body of a CONSTRUCT query results in valid queries, and produces the expected result bindings. This also entails that each BGP used to translate the data at query-time, using the statements in the mapping files, should be compatible with each other, which shoulld be non-trivial

### Reserach Questions: Recap
We are looking for a general Query Rewriting Mechanism which is able to rewrite an input query containing only variables and concepts occuring in either the Golden Agents ontology or the target knowledge graph, that is able to produce a CONSTRUCT query which generates a graph containing all concepts and relations from the original query, such that the original query can be evaluated on it.

In your research you will:

* Formalize this problem statement, including formalizing the conditions on the solution
* Investigate a generalized rewriting mechanism able to deal with a large number of mapping statements, that meets the conditioned solution
* Implement the mechanism in the agents of the Golden Agents system
